###########
# Imports #
###########

import os
import shutil
import sys
import threading
import time
import tkinter as tk
import webbrowser
from sys import platform
from tkinter import filedialog, IntVar, ttk

import Purple_methods as purple  # @UnresolvedImport
import yaml


# Use purple package for win GUI version
#import purple as purple  # @UnresolvedImport 

###########
# Classes #
###########
class CreateToolTip(object):
    '''
    create a tool tip for a given widget
    '''

    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)

    def enter(self, *unused):
        location = self.widget.bbox("insert")
        x = location[0]
        y = location[1]
        x += self.widget.winfo_rootx() + 0
        y += self.widget.winfo_rooty() + 50
        # creates a top level window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tw, text=self.text, justify='left',
                       background='lightyellow', relief='solid', borderwidth=1,
                       font=("times", "8", "normal"))
        label.pack(ipadx=1)

    def close(self, *unused):
        if self.tw:
            self.tw.destroy()


class Std_redirector(object):

    def __init__(self, widget):
        self.widget = widget

    def write(self, string):
        global isProgress
        global offset
        global outputPath
        global startLogging
        if("\r" in string and not isProgress):
            self.widget.insert(tk.END, "\n" + string)
            self.widget.see(tk.END)
            isProgress = True
            offset = len(string)
            if(startLogging):
                with open(outputPath.get()+"logfile.txt", "a") as logfile:
                    logfile.write(string)
        elif("\r" in string and isProgress):
            self.widget.delete("end - " + str(offset) + " chars", "end")
            self.widget.insert(tk.END, string)     
            self.widget.see(tk.END)
            offset = len(string) + 1
            if(startLogging):
                with open(outputPath.get()+"logfile.txt", "a") as logfile:
                    logfile.write(string)
        else:
            self.widget.insert(tk.END, string)
            self.widget.see(tk.END)
            isProgress = False
            if(startLogging):
                with open(outputPath.get()+"/logfile.txt", "a") as logfile:
                    logfile.write(string)

        
    def flush(self):
        pass

###########
# Methods #
###########


def startPurple():
    global purpleThread
    global startLogging
    
    # Start Purple
    purpleThread = threading.Thread(target=purple.main, args=[configPath.get()])
    purpleThread.daemon = True
    purpleThread.start()
    
    # Start logging
    startLogging = True
    loggingThread = threading.Thread(target=logging)
    loggingThread.daemon = True
    loggingThread.start() 
    
    # Watch Purple to start and end Animation
    animationThread = threading.Thread(target=showAnimation)
    animationThread.daemon = True
    animationThread.start()
    root.after(0, updateFrames, 0)

def logging():
    global purpleThread
    global outputPath 
    global startLogging 
    
    # Watch Purple thread
    while(purpleThread.isAlive()):
        time.sleep(1)
    startLogging = False
    
    # Move log file to final directory
    all_subdirs = [outputPath.get()+d for d in os.listdir(outputPath.get()) if os.path.isdir(outputPath.get()+d)]
    latest_subdir = max(all_subdirs, key=os.path.getmtime)
    shutil.move(outputPath.get()+"logfile.txt", latest_subdir+"/"+"logfile.txt")
    
def showAnimation():
    global purpleThread
    
    # Start Animation
    loadingAni.grid(row=0, column=boxCol, columnspan=1, rowspan=1)
    
    # Watch Purple thread
    while(purpleThread.isAlive()):
        time.sleep(1)
    
    # Stop animation
    loadingAni.grid_forget()
    del(purpleThread)

    
def stopPurple():
    print("Please wait to end.")

        
def exePurpleMain():

    exec(open('Run_Purple.py').read())
    startLogging = False


def selectTarget():
    global setTargetButtonText
    global cfg
    path = filedialog.askopenfilename(initialdir="/", title="Select target file")
    print("path", path)

    splitPath = path.split(sep='/')
    cfg['purple']['targetFile'] = splitPath[len(splitPath) - 1]
    setTargetButtonText.set(splitPath[len(splitPath) - 1])
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)

        
def selectDBs():
    global dbPath
    global cfg
    path = filedialog.askdirectory(initialdir="/", title="Select database directory")
    cfg['purple']['path_DB'] = path
    dbPath.set(path + "/")
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)


def selectOutput():
    global outputPath
    global cfg
    path = filedialog.askdirectory(initialdir="/", title="Select output directory") + "/"
    cfg['purple']['path_output'] = path
    outputPath.set(path)
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)

        
def setThreshold(self):
    global threshold
    global cfg
    cfg['purple']['threshold'] = threshold.get()
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)

        
def setMinLen():
    global minLen
    global cfg
    cfg['purple']['min_len_peptides'] = int(minLen.get())
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True


def setMaxLen():
    global maxLen
    global cfg
    cfg['purple']['max_len_peptides'] = int(maxLen.get())
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True


def setTargetList():
    global targetList
    global setTargetButtonText
    global cfg
    cfg['purple']['target'] = targetList.get().replace(' ', '').split(sep=",")
    setTargetButtonText.set('Select file...')
    cfg['purple']['targetFile'] = ""
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True


def setTitle():
    global title
    global cfg
    cfg['purple']['comment'] = title.get()
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True


def setDB():
    global dbPath
    global cfg
    cfg['purple']['path_DB'] = dbPath.get()
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True


def setTarget():
    global setTargetButtonText
    global cfg
    cfg['purple']['targetFile'] = setTargetButtonText.get()
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True


def setOutput():
    global outputPath
    global cfg
    cfg['purple']['path_output'] = outputPath.get()
    with open(configPath.get(), 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True


def toggle():
    '''
    use
    t_btn.config('text')[-1]
    to get the present state of the toggle button
    '''
    global setTargetButtonText
    global cfg
    if t_btn.config('text')[-1] == 'Target list':
        t_btn.config(text='Target file')
        setTargetButton.grid(row=3, column=boxCol + 1, padx=10, pady=0)
        setTargetBox.grid(row=3, column=boxCol, padx=0, pady=0, sticky='we')   
        targetListBox.grid_forget()
    else:
        t_btn.config(text='Target list')
        setTargetButtonText.set('Select file...')
        cfg['purple']['targetFile'] = ''
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
        setTargetButton.grid_forget()
        setTargetBox.grid_forget()
        targetListBox.grid(row=3, column=boxCol, padx=0, pady=0, sticky='we')   

        
def togglePrintPeptide():
    global cfg
    if togglePeptides.config('relief')[-1] == 'sunken':
        togglePeptides.config(relief="raised")
        togglePeptides.config(background="light salmon")
        cfg['purple']['print_peptides'] = False
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        togglePeptides.config(relief="sunken")
        togglePeptides.config(background="lightgreen")
        cfg['purple']['print_peptides'] = True
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)

            
def toggleRemoveFragments():
    global cfg
    if toggleFragments.config('relief')[-1] == 'sunken':
        toggleFragments.config(relief="raised")
        toggleFragments.config(background="light salmon")
        cfg['purple']['removeFragments'] = False
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleFragments.config(relief="sunken")
        toggleFragments.config(background="lightgreen")
        cfg['purple']['removeFragments'] = True
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)

            
def toggleUpdateDB():
    global cfg
    if toggleUpdateDB.config('relief')[-1] == 'sunken':
        toggleUpdateDB.config(relief="raised")
        toggleUpdateDB.config(background="light salmon")
        cfg['purple']['update_DB'] = False
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleUpdateDB.config(relief="sunken")
        toggleUpdateDB.config(background="lightgreen")
        cfg['purple']['update_DB'] = True
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)

            
def toggleCheckTargets():
    global cfg
    if toggleCheckTargets.config('relief')[-1] == 'sunken':
        toggleCheckTargets.config(relief="raised")
        toggleCheckTargets.config(background="light salmon")
        cfg['purple']['i_am_not_sure_about_target'] = False
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleCheckTargets.config(relief="sunken")
        toggleCheckTargets.config(background="lightgreen")
        cfg['purple']['i_am_not_sure_about_target'] = True
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)

            
def toggleDistinction():
    global cfg
    if toggleDistinction.config('relief')[-1] == 'sunken':
        toggleDistinction.config(relief="raised")
        toggleDistinction.config(background="light salmon")
        cfg['purple']['leucine_distincion'] = False
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleDistinction.config(relief="sunken")
        toggleDistinction.config(background="lightgreen")
        cfg['purple']['leucine_distincion'] = True
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)

            
def toggleProline():
    global cfg
    if toggleProline.config('relief')[-1] == 'sunken':
        toggleProline.config(relief="raised")
        toggleProline.config(background="light salmon")
        cfg['purple']['proline_digestion'] = False
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleProline.config(relief="sunken")
        toggleProline.config(background="lightgreen")
        cfg['purple']['proline_digestion'] = True
        with open(configPath.get(), 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)


def updateFrames(ind):
    frame = frames[ind]
    if ind <= 2:
        ind += 1
    else:
        ind = 0
    loadingAni.configure(image=frame)
    root.after(120, updateFrames, ind)

    
def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


def clickAbout():
    global versionText
    global docuText
    global logo
    helpWindow = tk.Toplevel()
    if platform == "win32":
        helpWindow.iconbitmap(resource_path('icon/Purple_64_basic.ico'))
    if os.name != "posix":
        tk.Label(helpWindow, image=logo).pack()
    version = tk.Label(helpWindow, text=versionText.get(), height=0, width=50)
    version.pack()
    docu = tk.Label(helpWindow, text=docuText.get(), height=0, width=50)
    docu.pack()
    lbl = tk.Label(helpWindow, text=r"https://gitlab.com/HartkopfF/Purple", fg="blue", cursor="hand2")
    lbl.pack()
    lbl.bind("<Button-1>", callback)

    
def loadConfig():
    global cfg
    global root
    path = filedialog.askopenfilename(initialdir="/", title="Select config file")
    configPath.set(path)
    with open(configPath.get(), 'r') as ymlfile:
        cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    print('Loading configurations...')
    print(configPath.get())
    for section in cfg:
        for entry in cfg[section]:
            print('\t', entry, ':', cfg[section][entry])
    root.title(versionText.get() + " - " + configPath.get())

    
def saveConfig():
    global cfg
    path = filedialog.asksaveasfilename(initialdir="/", title="Select location")
    with open(path, 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)


def callback(event):  # @UnusedVariable
    webbrowser.open_new(r"https://gitlab.com/HartkopfF/Purple")

###################
# Root properties #
###################


root = tk.Tk()
versionText = tk.StringVar()
versionText.set("Purple 0.4.2")
docuText = tk.StringVar()
docuText.set("Please visit our Gitlab page \n for help and documentation.")
configPath = tk.StringVar()
configPath.set(resource_path("config/config.yml"))
startLogging = False
if platform == "win32":
    root.iconbitmap(resource_path('icon/Purple_64_basic.ico'))
root.title(versionText.get() + " - " + configPath.get())

# root.geometry("780x380") #You want the size of the app to be 500x500
root.resizable(0, 0)  # Don't allow resizing in the x or y direction

# Column of name labels
namesCol = 7
# Column of entry boxes
boxCol = 9
# Width of toggles
toggleWidth = 20

isProgress = False

# Top bar icon
# @TODO: Fix MacOS version
if(os.name!="posix"):
    iconPath = resource_path("icon/Purple_128.png")
    logo = tk.PhotoImage(file=iconPath)
    w1 = tk.Label(root, image=logo).grid(row=0, column=boxCol, columnspan=1, rowspan=1)

# Loading animation
frames = [tk.PhotoImage(file=resource_path('icon/loading_128.gif'), format='gif -index %i' % (i)) for i in range(4)]
loadingAni = tk.Label(root)

############
# Menu bar #
############
menubar = tk.Menu(root)

# create a pulldown menu, and add it to the menu bar
filemenu = tk.Menu(menubar, tearoff=0)
filemenu.add_command(label="Load configuration", command=loadConfig)
filemenu.add_command(label="Save configuration", command=saveConfig)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=root.quit)
menubar.add_cascade(label="File", menu=filemenu)

# create more pulldown menus
helpmenu = tk.Menu(menubar, tearoff=0)
helpmenu.add_command(label="About", command=clickAbout)
menubar.add_cascade(label="Help", menu=helpmenu)

# display the menu
root.config(menu=menubar)

# Console
scrollbar = tk.Scrollbar(root, orient="vertical",)
scrollbar.grid(row=17, column=17, sticky='ns', columnspan=1, rowspan=16)
text = tk.Text(root, yscrollcommand=scrollbar.set, bg="black", fg="white")
sys.stdout = Std_redirector(text)
text.grid(row=17, column=0, columnspan=16, rowspan=16)

# config
with open(configPath.get(), 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
print('Loading configurations...')
print(configPath.get())
for section in cfg:
    for entry in cfg[section]:
        print('\t', entry, ':', cfg[section][entry])

# Start and Stop
purpleThread = threading.Thread(target=purple.main, args=[configPath.get()])
# stop_button = ttk.Button(root,text='Cancel',command=stopPurple)
# stop_button.grid(row=14,column=boxCol, padx=50, pady=10, sticky='e')
start_button = ttk.Button(root, text='Start Purple', command=startPurple)
start_button.grid(row=14, column=boxCol, padx=0, pady=10, sticky='we')

##########
# Config #
##########

# Path to DBs
dbPath = tk.StringVar()
dbPath.set(cfg['purple']['path_DB'])
browseText = tk.StringVar()
browseText.set("Browse...")
tk.Label(root, text="Path to DBs:").grid(row=1, column=namesCol, padx=0, pady=0, sticky='w')
setDBButton = ttk.Button(root, textvariable=browseText, command=selectDBs)
setDBButton.grid(row=1, column=boxCol + 1, padx=10, pady=0)
setDBBox = ttk.Entry(root, width=50, textvariable=dbPath, validate='focus', validatecommand=setDB)
setDBBox.grid(row=1, column=boxCol, padx=0, pady=0, sticky='we')   
setDB_ttp = CreateToolTip(setDBButton, "Select database folder containing target and background fasta databases for the Purple analysis.")

# Title
titleLabel = tk.Label(root, text="Title:").grid(row=2, column=namesCol, padx=0, pady=0, sticky='w')
title = tk.StringVar()
title.set(cfg['purple']['comment'])
titleBox = ttk.Entry(root, width=50, textvariable=title, validate='focus', validatecommand=setTitle)
titleBox.grid(row=2, column=boxCol, padx=0, pady=0, sticky='we')   
title_ttp = CreateToolTip(titleBox, "Add title that is used for the output.")

# Toggle between target list and target file
t_btn = ttk.Button(text="Target file", width=12, command=toggle)
t_btn.grid(row=3, column=namesCol, sticky='w')

# Target file
setTargetButtonText = tk.StringVar()
setTargetButtonText.set(cfg['purple']['targetFile'])
if(setTargetButtonText.get() == ""):
    setTargetButtonText.set("Select file...")
setTargetButton = ttk.Button(root, textvariable=browseText, command=selectTarget)
setTargetButton.grid(row=3, column=boxCol + 1, padx=10, pady=0)
setTargetBox = ttk.Entry(root, width=50, textvariable=setTargetButtonText, validate='focus', validatecommand=setDB)
setTargetBox.grid(row=3, column=boxCol, padx=0, pady=0, sticky='we')   
setTarget_ttp = CreateToolTip(setTargetButton, "Select a target database from the databases selected above. ")

# Target list
targetList = tk.StringVar()
targetListBox = ttk.Entry(root, width=30, textvariable=targetList, validate='focus', validatecommand=setTargetList)
targetList_ttp = CreateToolTip(targetListBox, "Provide a list of targets separated by a comma. These names are used to identify the target and can be subsequences of the target species.")

# Threshold
thresholdLabel = tk.Label(root, text="Background consensus threshold:").grid(row=4, column=namesCol, padx=0, pady=0, sticky='w')
threshold = IntVar()
threshold.set(int(cfg['purple']['threshold']))
thresholdScale = tk.Scale(root, from_=0, to=100, orient='horizontal', length=100, variable=threshold, command=setThreshold)
thresholdScale.grid(row=4, column=boxCol, padx=0, pady=0, columnspan=1, sticky='we')
Scale_ttp = CreateToolTip(thresholdScale, "Specify the alignment threshold for the homologous matching.")

# Sequence length
tk.Label(root, text="Sequence length:").grid(row=5, column=namesCol, padx=0, pady=0, sticky='w')
minLen = tk.StringVar()
minLen.set(cfg['purple']['min_len_peptides'])
maxLen = tk.StringVar()
maxLen.set(cfg['purple']['max_len_peptides'])
minLenBox = ttk.Entry(root, width=6, textvariable=minLen, validate='focus', validatecommand=setMinLen)
minLenBox.grid(row=5, column=boxCol, padx=70, pady=0, sticky='w')
tk.Label(root, text="to").grid(row=5, column=boxCol, padx=0, pady=0)
maxLenBox = ttk.Entry(root, width=6, textvariable=maxLen, validate='focus', validatecommand=setMaxLen)
maxLenBox.grid(row=5, column=boxCol, padx=70, pady=0, sticky='e')
minLen_ttp = CreateToolTip(minLenBox, "Specify minimum length of peptides.")
maxLen_ttp = CreateToolTip(maxLenBox, "Specify maximum length of peptides.")

# Print peptides
tk.Label(root, text="Additional options:").grid(row=6, column=namesCol, padx=0, pady=0, sticky='w')
if(cfg['purple']['print_peptides']):
    togglePeptides = tk.Button(text="Print peptides", width=toggleWidth, relief="sunken", background="lightgreen", command=togglePrintPeptide)
else:
    togglePeptides = tk.Button(text="Print peptides", width=toggleWidth, relief="raised", background="light salmon", command=togglePrintPeptide)
togglePeptides.grid(row=6, column=boxCol, sticky="w", pady=5)
printPeptides_ttp = CreateToolTip(togglePeptides, "Print peptides at the end of Purple.")

# Remove fragments
if(cfg['purple']['removeFragments']):
    toggleFragments = tk.Button(text="Remove fragments", width=toggleWidth, relief="sunken", background="lightgreen", command=toggleRemoveFragments)
else:
    toggleFragments = tk.Button(text="Remove fragments", width=toggleWidth, relief="raised", background="light salmon", command=toggleRemoveFragments)
toggleFragments.grid(row=6, column=boxCol, sticky="e", pady=5)
removeFragments_ttp = CreateToolTip(toggleFragments, "Option to remove proteins with \"(Fragments)\" in the header.")

# Update database
if(cfg['purple']['update_DB']):
    toggleUpdateDB = tk.Button(text="Update database", width=toggleWidth, relief="sunken", background="lightgreen", command=toggleUpdateDB)
else:
    toggleUpdateDB = tk.Button(text="Update database", width=toggleWidth, relief="raised", background="light salmon", command=toggleUpdateDB)
toggleUpdateDB.grid(row=7, column=boxCol, sticky="w", pady=5)
updateDB_ttp = CreateToolTip(toggleUpdateDB, "Build a new database or use the old one.")

# Check targets
if(cfg['purple']['i_am_not_sure_about_target']):
    toggleCheckTargets = tk.Button(text="Check targets", width=toggleWidth, relief="sunken", background="lightgreen", command=toggleCheckTargets)
else:
    toggleCheckTargets = tk.Button(text="Check targets", width=toggleWidth, relief="raised", background="light salmon", command=toggleCheckTargets)
toggleCheckTargets.grid(row=7, column=boxCol, sticky="e", pady=5)
checkTargets_ttp = CreateToolTip(toggleCheckTargets, "Option to check targets before matching peptides.")

# Leucine/Isoleucine distinction
if(cfg['purple']['leucine_distinction']):
    toggleDistinction = tk.Button(text="Isoleucine distinction", width=toggleWidth, relief="sunken", background="lightgreen", command=toggleDistinction)
else:
    toggleDistinction = tk.Button(text="Isoleucine distinction", width=toggleWidth, relief="raised", background="light salmon", command=toggleDistinction)
toggleDistinction.grid(row=9, column=boxCol, sticky="e", pady=5)
distinction_ttp = CreateToolTip(toggleDistinction, "Enable or disable the distinction of leucine and isoleucine")

# Proline digestion
if(cfg['purple']['proline_digestion']):
    toggleProline = tk.Button(text="Proline digestion rule", width=toggleWidth, relief="sunken", background="lightgreen", command=toggleProline)
else:
    toggleProline = tk.Button(text="Proline digestion rule", width=toggleWidth, relief="raised", background="light salmon", command=toggleProline)
toggleProline.grid(row=9, column=boxCol, sticky="w", pady=5)
proline_ttp = CreateToolTip(toggleProline, "Allow digestion if a proline follows on a arginine or lysine.")

# Path to output
outputPath = tk.StringVar()
outputPath.set(cfg['purple']['path_output'])
tk.Label(root, text="Path to output:").grid(row=13, column=namesCol, padx=0, pady=0, sticky='w')
setOutputButton = ttk.Button(root, textvariable=browseText, command=selectOutput)
setOutputButton.grid(row=13, column=boxCol + 1, padx=10, pady=0)
setOutputBox = ttk.Entry(root, width=50, textvariable=outputPath, validate='focus', validatecommand=setOutput)
setOutputBox.grid(row=13, column=boxCol, padx=0, pady=0, sticky='e')
setOutput_ttp = CreateToolTip(setOutputButton, "Select output folder for the Purple results.")

root.mainloop()
